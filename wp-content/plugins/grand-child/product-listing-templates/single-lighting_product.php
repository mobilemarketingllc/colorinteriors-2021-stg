<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 <?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<div class="product-detail-layout-6">
			<?php
				global $post;
				$meta_values = get_post_meta( get_the_ID() );
				// $brand = $meta_values['brand'][0] ;
				// $sku = $meta_values['sku'][0];
				// $manufacturer = $meta_values['manufacturer'][0];
				// $collection = $meta_values['collection'][0];    
			?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 col-sm-12 product-swatch">   
					<?php 
						$image = $meta_values['product_image'][0];
					?>	
					<img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
					<div class="clearfix"></div>
				</div>
				<div class="col-md-5 col-sm-12 product-box">
					<div class="row">
						<div class="col-md-12">
							<?php if(array_key_exists("item_name",$meta_values)){?>
								<h1 class="fl-post-title" itemprop="name"><?php echo $meta_values['item_name'][0]." (".$meta_values['item_number'][0]." )"; ?></h1>
							<?php } ?>

							<p><?php echo $meta_values['short_description'][0]; ?></p>
							<hr/>
							
							<h4>Product Specification</h4>
							<table class="table-reveal" id="table-reveal-1">
								<tr>
									<th>Item #</th>
									<td><?php echo @$meta_values['item_number'][0];?></td>
								</tr>

								<?php if(@$meta_values['width'][0] || @$meta_values['height'][0] ){ ?>
									<tr>
										<th>Dimensions</th>
										<td><?php if(@$meta_values['height'][0] ){?>
											<?php echo "Height: ".$meta_values['height'][0]."<br>";?>
											<?php } ?>
											<?php if(@$meta_values['width'][0] ){?>
											<?php echo "Width: ".$meta_values['width'][0];?>
											<?php } ?>
										</td>
									</tr>
								<?php }	?>
								<!-- <?php if(@$meta_values['variant_data'][0] ){ ?>
									<tr>
										<th>Finish</th>
										<td><?php echo str_replace("|","<br>", str_replace("\"","",$meta_values['variant_data'][0]));?>
										</td>
									</tr>
								<?php }	?> -->

								<?php if(@$meta_values['standard_category'][0] ){ ?>
									<tr>
										<th>Category</th>
										<td><?php echo $meta_values['standard_category'][0];?>
										</td>
									</tr>
								<?php }	?>
								
								<tr class="buttonTOshow" >
									<th colspan="2"> Additional product details <i class="fa fa-chevron-down" aria-hidden="true"></i> </th>
								</tr>

								<?php if(@$meta_values['standard_subcategory'][0] ){ ?>
									<tr>
										<th>Subcategory</th>
										<td><?php echo $meta_values['standard_subcategory'][0];?>
										</td>
									</tr>
								<?php }	?>

								<?php if(@$meta_values['standard_finish'][0] ){ ?>
									<tr>
										<th>Finish</th>
										<td><?php echo $meta_values['standard_finish'][0];?>
										</td>
									</tr>
								<?php }	?>

								<?php if(@$meta_values['standard_style'][0] ){ ?>
									<tr>
										<th>Style</th>
										<td><?php echo $meta_values['standard_style'][0];?>
										</td>
									</tr>
								<?php }	?>

								<?php if(@$meta_values['height'][0] ){ ?>
									<tr>
										<th>Height</th>
										<td><?php echo $meta_values['height'][0];?>
										</td>
									</tr>
								<?php }	?>

								<?php if(@$meta_values['width'][0] ){ ?>
									<tr>
										<th>Width</th>
										<td><?php echo $meta_values['width'][0];?>
										</td>
									</tr>
								<?php }	?>

								<?php $extra_data = $meta_values['variant_data'][0]; 
									if($extra_data){
										$edatas = explode("|", $extra_data);
										if(is_array($edatas) && count($edatas) > 0){
											foreach($edatas as $edata){
												$edata_val = explode(":", $edata);
												if(is_array($edata_val) && count($edata_val) > 0){
													?>
													<tr>
														<th><?php echo trim($edata_val[0],"\"");?></th>
														<td><?php echo trim($edata_val[1],"\"");;?>
														</td>
													</tr>
													<?php
												}
											}
										}
									}
								?>
							</table>
							

							<?php if(@$meta_values['required_items'][0] ){ ?>
								<hr>
								<h4>Recommended Items</h4>
								<p><?php echo $meta_values['required_items'][0];?></p>
							<?php }	?>

							<?php if(@$meta_values['price'][0] ){ ?>
								<hr>
								<h2><sup>$</sup><?php echo $meta_values['price'][0];?></h2>
							<?php }	?>

							<?php if(@$meta_values['in_stock'][0] ){ ?>
								<br>
								<h4>Availability:</h4>
								<p><?php echo "Manufacturer has ".$meta_values['in_stock'][0]." items available.";?></p>
							<?php }	?>

 						</div>							
						
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="product-attributes-wrap">
						<?php 
							// $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
							// include( $dir );
						?>
					</div>		
				</div>
			</div>			

				<div class="clearfix"></div>
			
			
		</div>
	</article>
</div>	







			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>
<script>

	(function() {

	const tableReveal = function(elm, options) {
	  // merge options
	  options = Object.assign({}, {
	    limit: 3
	  }, options)

	  // the tr's
	  let trs = elm.querySelectorAll('tbody tr')

	  // shown state
	  let shown

	  // funcs
	  const hide = () => {
	    trs.forEach((tr, index) => index >= options.limit ? tr.style.display = 'none' : '')
	    shown = false
	  }

	  const show = () => {
	    trs.forEach((tr) => tr.style.display = 'table-row')
	    shown = true
	  }

	  // initial state
	  hide()

	  // reveal funcs
	  return {
	    toggle: () => shown ? hide() : show(),
	    hide,
	    show
	  }
	}

	/**
	 * Usage:
	 * - For each over every table-reveal class, init reveal func and pass in element + options, return and assign table[id] for button
	 */
	let table = {}
	document.querySelectorAll('.table-reveal').forEach(el => table[el.getAttribute('id')] = tableReveal(el, {
	  limit: 4
	}))

	document.querySelector('.buttonTOshow').addEventListener("click", function(){
		table['table-reveal-1'].toggle();
	}); 	
	})();

	
    jQuery(document).ready(function(){
        var rows = jQuery(".table-reveal tbody tr").length;
        if(rows <= 2){
        	jQuery(".table-reveal tbody .buttonTOshow").hide();
        }
    });
</script> 
<?php get_footer(); ?>