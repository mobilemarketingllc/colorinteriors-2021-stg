<?php

trait WP_Example_Logger {

	/**
	 * Really long running process
	 *
	 * @return int
	 */
	public function really_long_running_task() {
		return sleep( 1 );
	}

	/**
	 * Log
	 *
	 * @param string $message
	 */
	public function log( $message ) {
		error_log( $message );
	}

	/**
	 * Get lorem
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function get_message( $name,$id ) {

		$msg = $name.'-'.$id.' Inserted Successfully' ;
		
		return $msg;
	}

	/**
	 * Insert Product 
	 *
	 * @param string $message
	 */
	 public function insert_product( $data ,$main_category) {

		global $wpdb;

		

		$table_redirect = $wpdb->prefix.'redirection_items';
		$table_group = $wpdb->prefix.'redirection_groups';		
		$table_posts = $wpdb->prefix.'posts';
		$table_meta = $wpdb->prefix.'postmeta';	
		$satur = array('Masland','Dixie Home');

		write_log('----------'.$data['sku'].'-------------');

		//write_log($data);

		if($data['status']=='active' &&  trim(@$data['swatch'])!="" ){

			//condition true when product status ia active

			write_log($main_category);

					// Set post data as per fields in ACF

					// if (in_array($data['brand'], $satur) || $data['brand'] == 'Dream Weaver' || $data['brand'] == 'Marazzi'){
					// 	$data['collection'] = @$data['design'] ;	
					// }
					// else{
					// 	$data['collection'] = @$data['collection_name'] ;	
					// }

					$data['collection'] = @$data['collection_name'] ;	
						
					$data['installation_method'] = @$data['installation'] ;
					$data['warranty_info'] =  @$data['warranty_text']  ;
					$data['swatch_image_link'] = $data['swatch'] ;
					$data['gallery_room_images'] = $data['gallery_images'] ;

					
					$data['brand_collection'] = $data['brand']." ".$data['collection'];	
					unset($data['installation'] );
					unset($data['warranty_text'] );
					unset($data['swatch'] );
					unset($data['gallery_images'] );


					$data = array_filter($data);
					
					// args for checking already inserted product
					// find list of states in DB

						$find_sku = $data['sku'];

						$sql_sku = "SELECT $table_meta.post_id 
						FROM $table_meta
						WHERE  $table_meta.meta_key = 'sku' 
						AND $table_meta.meta_value = '$find_sku'" ;	
						
						$duplicates = $wpdb->get_results($sql_sku,ARRAY_A);							
						
																		
						// do something if the sku exists in another post
						if (count($duplicates)> 0){
							
							// do your stuff

								write_log('Already-'.$data['sku'].'->'.$duplicates[0]['post_id']);
								
								$post_id = $duplicates[0]['post_id'];									
								
								
								foreach($data as $key=>$value){

									if($value != '' || $value != null){

										//update sale post meta data
									update_post_meta($post_id, $key, $value); 

									}

									
								}	

								update_post_meta($post_id, 'endpoint', '1'); 
								
								// Insert the post into the database.
								$up_post = array(
									'ID'           => $post_id									
								);	
								
								$post_id = wp_update_post( $up_post );	

								$wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['sku'].'"');

								$wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['sku'].'%"');


						}else{
							
									$my_post = array(
										'post_title'    => $data['name'].' '.$data['sku'],
										'post_content'  => '',
										'post_type'  => $main_category,
										'post_status'   => 'publish',
										'post_author'   => 1,	
										'meta_input'   => $data,
						
									);		
						   
							  $wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['sku'].'"');

							  $wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['sku'].'%"');							

							   									
								// Insert the post into the database.
								$post_id = wp_insert_post( $my_post );

								update_post_meta($post_id, 'endpoint', '1'); 
									
								write_log('New-'.$post_id." SUKD: ".$data['sku']);

								//	write_log('----------'.$data['sku'].'----------');
						}
			
					}else{
				
				
						//COndition true if status of product is inactive or dropped or gone or deleted
						//	args to query for sku which deleted from api
						$args = array(
							'post_type' => $main_category,
							'meta_query' => array(
								array(
									'key' => 'sku',
									'value' => $data['sku']
								)
							),
							'fields' => 'ids'
						);
						// perform the query
						$query = new WP_Query( $args );
						$deleted = $query->posts;
						if ( ! empty( $deleted ) ) {
		
						
						//write_log(' Deleted this product -'.$deleted['0'].' SKU -'.$data['sku']);
		
						$brandmapping = array(
							"/flooring/carpet/products/"=>"carpeting",
							"/flooring/hardwood/products/"=>"hardwood_catalog",
							"/flooring/laminate/products/"=>"laminate_catalog",
							"/flooring/"=>"luxury_vinyl_tile",
							"/flooring/tile/products/"=>"tile_catalog",
							"/flooring/waterproof/"=>"solid_wpc_waterproof"
						);
						

					   $destination_url = array_search($main_category,$brandmapping);
		
					   $source_url = wp_make_link_relative(get_permalink($deleted['0']));

					   $match_url = rtrim($source_url, '/');
		
					   //write_log($product_permalink);
					  
					   $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
        				$redirect_group =  $datum[0]->id;

					   $data = array("url" => $source_url,
					   "match_url" => $match_url,
					   "match_data" => "",
					   "action_code" => "301",
					   "action_type" => "url",
					   "action_data" => $destination_url,
					   "match_type" => "url",
					   "title" => $data['sku'],
					   "regex" => "true",
					   "group_id" => $redirect_group,
					   "position" => "1",
					   "last_access" => current_time( 'mysql' ),
					   "status" => "enabled");

					   $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

					  // $wpdb->insert($table_redirect,$data,$format);


						
						// write_log(' Added 301 redirect -'.$deleted['0'].' SKU -'.$data['sku']);

						// wp_delete_post( $deleted['0']);
		
						 write_log('----------'.$data['sku'].'----------');
						}
		
		
					}
				//	exit;
		 }

	/**
	 * Insert Product CSV
	 *
	 * @param string $message
	 */
	 public function insert_product_csv( $data ,$main_category) {

		global $wpdb;

		$table_redirect = $wpdb->prefix.'redirection_items';
		$table_group = $wpdb->prefix.'redirection_groups';		

		if($data['status']=='active' && trim($data['swatch'])!="" ){

			write_log($main_category);

			// args for checking already inserted product
			// find list of states in DB	
					 
			$post_id =0;

			// args to query for your sku checking
			$args = array(
				'post_type' => $main_category,
				'meta_query' => array(
					array(
						'key' => 'sku',
						'value' => $data['sku']
					)
				),
				'fields' => 'ids'
			);
			// perform the query
			$query = new WP_Query( $args );
			$duplicates = $query->posts;

			if($duplicates['0']){
			write_log($duplicates['0']);
			}


		 if ( ! empty( $duplicates ) ) {
			 
			write_log('Already-'.$data['sku'].'->'.$duplicates[0]);
			
			$post_id = $duplicates['0'];
			write_log("Updated POST ID $post_id",$duplicates['0']);
			$del = "DELETE FROM {$wpdb->postmeta} WHERE post_id = '".$post_id."'";
			write_log('----------'.$data['sku'].'----------');
			$wpdb->query( $del );

		 }else{			

		// Insert post data.

		$wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['sku'].'"');

		$wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['sku'].'%"');

		write_log($wpdb->last_query);

			$my_post = array(
				'post_title'    => $data['name'].' '.$data['sku'],
				'post_content'  => '',
				'post_type'  => $main_category,
				'post_status'   => 'publish',
				'post_author'   => 1,				
			);

			
			
			// Insert the post into the database.
			$post_id = wp_insert_post( $my_post );
			write_log('New-'.$post_id);
		
			//$wpdb->insert( $product_check_table, array('skuid' => $data['sku'], 'post_id' =>$post_id), array( '%s', '%s'));
			
		}
		
		$values="";
    foreach($data as $key => $value){
			$arryColumnNotREquired= array("name","post_type");
				if( !in_array($key, $arryColumnNotREquired)){
					if(isset($value) && $value !=""){
						//write_log("VAl::", $key,$value);

						if($key == 'collection_name' || $key == 'brand'){
							$brand_collection .= " ".$value;
						}
						
						if($key == 'collection_name'){
							$values.= "(".$post_id.",'collection','".wp_slash($value)."')," ;
						}
						if( $key == 'in_stock'){
							$values.= "(".$post_id.",'construction_facet','".wp_slash($value)."')," ;
						}
						if( $key == 'installation'){
							$values.= "(".$post_id.",'installation_method','".wp_slash($value)."')," ;
						}
						if( $key == 'warranty_text'){
							$values.= "(".$post_id.",'warranty_info','".wp_slash($value)."')," ;
						}
						if( $key == 'swatch'){
							$values.= "(".$post_id.",'swatch_image_link','".wp_slash($value)."')," ;
						}
						if( $key == 'gallery_images'){
							$values.= "(".$post_id.",'gallery_room_images','".wp_slash($value)."')," ;
						}
						else{
							$values.= "(".$post_id.",'".$key."','".wp_slash($value)."')," ;
						}

							
					}
				}
			
		} 

		if($brand_collection !=""){
			$values.= "(".$post_id.",'brand_collection','".wp_slash($brand_collection)."')," ;
		}
	
	 return $values;
	 return $post_id;
		
	}
	 }
		


	 /**
	 * Insert Lighting Product CSV
	 *
	 * @param string $message
	 */
	public function insert_lighting_product_csv( $data ,$main_category) {

		global $wpdb;

		$table_redirect = $wpdb->prefix.'redirection_items';
		$table_group = $wpdb->prefix.'redirection_groups';		

		
			$post_id =0;

			// args to query for your sku checking
			$args = array(
				'post_type' => $main_category,
				'meta_query' => array(
					array(
						'key' => 'item_number',
						'value' => $data['Item Number']
					)
				),
				'fields' => 'ids'
			);
			// perform the query
			$query = new WP_Query( $args );
			$duplicates = $query->posts;

			if($duplicates['0']){
				write_log($duplicates['0']);
			}
			if(@$data['Standard-Category']){
				$cat  = get_term_by('name', $data['Standard-Category'] , 'lighting_category');
				//check existence
				if($cat == false){
					$cat = wp_insert_term($data['Standard-Category'], 'lighting_category');
					write_log($cat);
					// $cat_id = $cat['term_id'] ;
				}else{
					$cat_id = $cat->term_id ;
				}
			}


			if ( ! empty( $duplicates ) ) {
				
				write_log('Already-'.$data['Item Number'].'->'.$duplicates[0]);
				
				$post_id = $duplicates['0'];
				if(@$data['Standard-Category']){
					wp_set_post_terms($post_id,array($cat_id),'lighting_category');
				}
				// wp_set_post_terms( $post_id, $data['Standard-Category'], 'lighting_category' );
			
				write_log("Updated POST ID $post_id",$duplicates['0']);
				$del = "DELETE FROM {$wpdb->postmeta} WHERE post_id = '".$post_id."'";
				write_log('----------'.$data['Item Number'].'----------');
				$wpdb->query( $del );

			}else{			

				// Insert post data.

				$wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['Item Number'].'"');

				$wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['Item Number'].'%"');

				write_log($wpdb->last_query);

				$my_post = array(
					'post_title'    => $data['Item Name'].' '.$data['Item Number'],
					'post_content'  => '',
					'post_type'  => $main_category,
					'post_status'   => 'publish',
					'post_author'   => 1,				
				);

				// Insert the post into the database.
				$post_id = wp_insert_post( $my_post );
				if(@$data['Standard-Category']){
					wp_set_post_terms($post_id,array($cat_id),'lighting_category');
				}
				write_log('New-'.$post_id);
			
				//$wpdb->insert( $product_check_table, array('skuid' => $data['sku'], 'post_id' =>$post_id), array( '%s', '%s'));
			
			}
		
			$values="";
			foreach($data as $key => $value){
				$arryColumnNotREquired= array("name","post_type");
				if( !in_array($key, $arryColumnNotREquired)){
					if(isset($value) && $value !=""){
						//write_log("VAl::", $key,$value);
						if($key == 'brand_facet'){
							$values.= "(".$post_id.",'brand','".wp_slash($value)."')," ;
						}elseif( $key == 'Item Number'){
							$values.= "(".$post_id.",'item_number','".wp_slash($value)."')," ;
						}elseif( $key == 'Item Name'){
							$values.= "(".$post_id.",'item_name','".wp_slash($value)."')," ;
						}elseif( $key == 'Short Description'){
							$values.= "(".$post_id.",'short_description','".wp_slash($value)."')," ;
						}elseif( $key == 'Long Description'){
							$values.= "(".$post_id.",'long_description','".wp_slash($value)."')," ;
						}elseif( $key == 'Price'){
							$values.= "(".$post_id.",'price','".wp_slash($value)."')," ;
						}elseif( $key == 'In Stock'){
							$values.= "(".$post_id.",'in_stock','".wp_slash($value)."')," ;
						}elseif( $key == 'Width'){
							$values.= "(".$post_id.",'width','".wp_slash($value)."')," ;
						}elseif( $key == 'Height'){
							$values.= "(".$post_id.",'height','".wp_slash($value)."')," ;
						}elseif( $key == 'Required Items'){
							$values.= "(".$post_id.",'required_items','".wp_slash($value)."')," ;
						}elseif( $key == 'Standard-Category'){
							$values.= "(".$post_id.",'standard_category','".wp_slash($value)."')," ;
						}elseif( $key == 'Standard-Subcategory'){
							$values.= "(".$post_id.",'standard_subcategory','".wp_slash($value)."')," ;
						}elseif( $key == 'Standard-Finish'){
							$values.= "(".$post_id.",'standard_finish','".wp_slash($value)."')," ;
						}elseif( $key == 'Standard-Style'){
							$values.= "(".$post_id.",'standard_style','".wp_slash($value)."')," ;
						}elseif( $key == 'Variant Data'){
							$values.= "(".$post_id.",'variant_data','".wp_slash($value)."')," ;
						}elseif( $key == 'Extra Data'){
							$values.= "(".$post_id.",'extra_data','".wp_slash($value)."')," ;
						}elseif( $key == 'Image Path'){
							$values.= "(".$post_id.",'product_image','".wp_slash($value)."')," ;
						}else{
							$values.= "(".$post_id.",'".$key."','".wp_slash($value)."')," ;
						}
					}

				}
			
			} 

			
	 		return $values;
		
	}
}